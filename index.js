const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const {dbConnection} = require('./db/config');
const PORT = process.env.PORT || 3000;
// crear servidor
const app = express();
app.use(cors());
//lectura y parseo del body
app.use(express.json());
// app.use(fileUpload());
// **********************************  rutas  ************************************
app.get('/',(req,res) =>{
    res.json({
        ok: true,
        message:"Bienvenido"
    })
})

app.get('/Bouchers/img/:tipo',(req,res) =>{
    const tipo = req.params.tipo
    res.json({
        ok: true,
        message:"imgs",
        tipo
    })
})
//ruta nuevo presidente

// app.use( require('./routes/presidentes/presi'));
 const path = require('path');
app.use(require('./routes/indexrutas'));


 app.use(express.static(path.join(__dirname, './Bouchers')));

//******************************************************************************** */
//db connection
dbConnection();
app.listen(PORT, () => {
    console.log("servidor backend corriendo puerto");
});