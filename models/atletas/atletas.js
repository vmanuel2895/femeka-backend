const { Schema, model } = require('mongoose');

const AtletaShema = Schema({
    nombre:{type : String,default:"", uppercase: true},
    apellidoPaterno:{type : String,default:"", uppercase: true},
    apellidoMaterno:{type : String,default:"", uppercase: true},
    curp:{type : String,default:"", uppercase: true},
    edad:{type : String,default:""},
    sexo:{type : String,default:"", uppercase: true},
    estado:{type : String,default:"", uppercase: true},
    modalidad:{type : String,default:"" },
    integrantes:{type : String,default:""},
    colorCinta:{type : String,default:""},
    categoria:{type : String,default:""},
    equipo:{type : String,default:""},
    division:{type : String,default:""},
    presidente:{type : String,default:""}
});
module.exports = model ('Atletas', AtletaShema);