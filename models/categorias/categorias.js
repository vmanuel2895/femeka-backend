const { Schema, model} = require('mongoose');

const categoriaSchema = Schema({
    categoria:{ type: String},
    genero:{ type: String},
    edadMin:{type:Number},
    edadMax:{type:Number},
    integrantes:{type:String}
});

module.exports = model('categorias', categoriaSchema);