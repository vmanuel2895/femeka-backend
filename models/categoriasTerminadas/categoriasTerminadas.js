const {Schema, model} = require('mongoose');

const CateTerminada = Schema({
    categoria:{type: String},
    division:{type:String},
    rango: {type: String},
    genero: {type:String}
});

module.exports = model('CategoriasTerminadas', CateTerminada)