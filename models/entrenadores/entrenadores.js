const { Schema, model } = require('mongoose');

const EntrenaShema = Schema({
    apellidoMaterno:{type : String,default:"", uppercase: true},
    apellidoPaterno:{type : String,default:"", uppercase: true},
    curp:{type : String,default:"", uppercase: true},
    estado:{type : String,default:"", uppercase: true},
    estilo:{type : String,default:"", uppercase: true},
    fechaNacimiento:{type : String,default:""},
    gradoCN:{type : String,default:"", uppercase: true},
    nombre:{type : String,default:"", uppercase: true},
    sexo:{type : String,default:"", uppercase: true},
    presidente:{type : String,default:""}
});
module.exports = model ('Entrenadores', EntrenaShema);