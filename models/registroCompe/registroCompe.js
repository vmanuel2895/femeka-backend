const { Schema, model } = require('mongoose');

const CompeShema = Schema({
    categoria:{type : String,default:""},
    genero:{type : String,default:""},
    grado:{type : String,default:""},
    vueltas:{type: Number,default:1},
    equipoAKA:{type : Array},
    equipoAO:{type : Array},
    atletasSemi1:{type : Array},
    atletasSemi2:{type : Array},
    atletasFinal:{type : Array},
    resultados:{type:Array},
    semifinal1:{type : Array},
    semifinal2:{type : Array},
    final:{type : Array}
});
module.exports = model ('Competencias', CompeShema);