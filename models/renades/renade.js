const {Schema, model} = require('mongoose');

const renadeSchema = new Schema({
    idPresidente : {type: Schema.Types.ObjectId, ref:'Presidente'},
    folio: {type : String},
    estado: { type: String, uppercase: true},
    nombre: {type : String, uppercase: true},
    apellidoPaterno:{type: String, uppercase: true},
    apellidoMaterno:{type: String, uppercase: true},
    fechaNacimiento:{type : String},
    edad:{type: Number},
    genero:{type:String, uppercase: true},
    vigencia:{type: String},
    curp:{type: String, uppercase: true},
    tipo:{type: String, uppercase: true},
    club:{type: String, uppercase: true},
    directorClub:{type: String, uppercase: true},
    direccionClub:{type: String, uppercase: true},
    email:{type: String},
    telefono:{type: String}
});

module.exports = model ('Renades',renadeSchema);