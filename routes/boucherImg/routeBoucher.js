const {Router, response} = require('express');
const imgBou = require('../../models/bouchers/boucher');
const fileUpload = require('express-fileupload');
const path = require('path');
const fs = require('fs');
const {v4: uuidv4 } = require('uuid');
const app = Router();

app.use(fileUpload());
const multer = require('../../storage/boucherImg');

app.post('/agregar/boucher', multer.array('bouchers', 12), (req, res, next) => {
    const body = req.body;
    const imagenesTiket = [];
  console.log(req.files);
    req.files.forEach(img => {
        imagenesTiket.push(img.originalname);
    });
    console.log(imagenesTiket);

    const newReturnTiket = new imgBou({
        
        bouchers: imagenesTiket
       
    });

    newReturnTiket.save();
    res.json({
    ok:true,
      mns:"Boucher Guardados",
      newReturnTiket
    });
        // .then(data => serveResp(data, null, 'Se envió nuevo regreso xray', resp))
        // .catch(error => serveResp(null, error, 'No se encontró regreso xray', resp));

});



app.put('/subir/archivo/:tipo/:id/:presi', async(req,res)=>{

    const tipo = req.params.tipo;
    const presi = req.params.presi
const carpetaImg = ['img'];

if(!carpetaImg.includes(tipo)){
    res.json({
        ok:false,
          mns:"carpeta no asignada"
        });
}
//validar que exista archvio
if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).json({
        ok:false,
        msj:"no hay archivo"
    });
  }

  //procesar imagen
  const file = req.files.bouchers;
  const nombreCortado = file.name.split('.');
  const extencionArchivo = nombreCortado[nombreCortado.length -1];

  //extenciones validas

  const extencionValida = ['png','jpg','gif','jpeg'];
  if(!extencionValida.includes(extencionArchivo)){
    res.json({
        ok:true,
          mns:"No es una extencion existente"
        });
  }

  //Generar nombre del archivo
  const nomArchivo = `${ uuidv4()}.${extencionArchivo}`; 

  // path guardar imagen
const path =`./Bouchers/${tipo}/${ nomArchivo}`;
// const path =`./Bouchers/img/${nomArchivo}`;


// mover imagen
file.mv(path,(err)=>{
    if(err){
        console.log(err);
        return res.status(500).json({
           ok: false,
           msg:"Error al mover imagen"
        });
    }

 const r = new imgBou({ bouchers: path, presidente : presi})
 r.save();
    
    res.json({
        ok:true,
        mns:"Arvhico subido",
          nomArchivo,
          r
        });
})

});


app.get('/tikets', async(req,res)=>{

  const result = await imgBou.find();
  res.json({
    ok:true,
    result
  })
});



app.get('/ver/:tipo/:foto', (req,res = response)=>{
const tipo = req.params.tipo;
const foto = req.params.foto;

const pathImg = path.join( __dirname, `../../Bouchers/${tipo}/${foto}`);

//imagen por defecto
if (fs.existsSync(pathImg)){

  res.sendFile(pathImg);
} else {

  const pathImgN = path.join(__dirname, `../../Bouchers/no-found.jpg`);
  res.sendFile(pathImgN);
}


});

app.post('/subir/img/vocuher', (req,res) =>{
  let body = req.body;

  let voucher = new imgBou({
    bouchers : body.bouchers,
    presidente : body.presidente
  });

  voucher.save();

  res.json({
    ok:true,
    voucher
  })
});

// peticion del vico imagen por id de presidente

app.get('/obtener/imagen/presidente/:presidente', async (req,res) =>{
  const presi = req.params.presidente;

  const respuesta = await imgBou.find({presidente:presi});
 
  res.json({
    ok:true,
    msj:"Encontre Presidente con voucher",
    respuesta 
  });
 });
 
module.exports = app;