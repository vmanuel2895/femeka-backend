const {Router}= require('express');
const categoriaTabla = require('../../models/categorias/categorias');
const app = Router();

app.get('/obtener/categorias', async (req,res)=>{

    const body = req.body;

    const categorias = await categoriaTabla.find();

    if(categorias == "" || categorias == null){
        res.status(404).json({
            ok:false,
            msj:"No hay categorias",
        })
    }
    
    res.json({
     ok:true,
     msj:"Estos son los categorias",
     categorias
    });
});

app.post('/agregar/nueva/categoria',(req,res)=>{
  const body = req.body;
    const respuesta = new categoriaTabla({
        categorias : body.categorias
    });

    respuesta.save();
    res.json({
        ok:true,
        msj:"categoria enviada",
        respuesta
    })
});

app.post('/categorias/individual/atleta', async(req,res)=>{
    const body = req.body
    const categorias = await categoriaTabla.find({genero:body.genero})
    /* .find({genero:body.genero, $or: [{edadMin: {$gt: body.edad}}, {edadMax: {$lt: body.edad}}]}); */
    res.json({
        ok:true,
        msj:"Estos son los categorias",
        categorias
    });
})

module.exports = app;