const{Router}= require('express');
const kyu = require('../../models/categoriasKyu/kyu');
const app = Router();

app.post('/agregar/nuevo/grado/kyu',(req,res)=>{

    const body = req.body;

    const respuesta = new kyu({
     grado: body.grado
    });

    respuesta.save();

    res.json({
        ok:true,
        msj:"Grado enviado exitosamente",
        respuesta
    })

});


app.get('/obtener/grados/kyu',async(req,res)=>{

    const grados = await kyu.find();
res.json({
ok:true,
msj:"Estos son los grados kyu",
grados
});

});

module.exports = app;