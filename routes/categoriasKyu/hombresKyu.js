const {Router} = require('express');
const HKyu = require('../../models/categoriasKyu/hombres');
const app = Router();

app.post('/agregar/nuevo/grado/kyu/hombres',(req,res)=>{

    const body = req.body;

    const respuesta = new HKyu({
     categoria: body.categoria
    });

    respuesta.save();

    res.json({
        ok:true,
        msj:"Edad enviado exitosamente",
        respuesta
    })

});

app.get('/obtener/grados/kyu/Hombres',async(req,res)=>{

    const edades = await HKyu.find();
    res.json({
        ok:true,
        msj:"Estos son los grados kyu",
        edades
    });

});


module.exports = app;