const {Router} = require('express');
const Mkyu = require('../../models/categoriasKyu/mujeres');
const app = Router();

app.post('/agregar/nuevo/grado/kyu/mujeres',(req,res)=>{

    const body = req.body;

    const respuesta = new Mkyu({
     edad: body.edad
    });

    respuesta.save();

    res.json({
        ok:true,
        msj:"Edad enviado exitosamente",
        respuesta
    })

});

app.get('/obtener/grados/kyu/mujeres',async(req,res)=>{

    const edades = await Mkyu.find();
res.json({
ok:true,
msj:"Estos son los grados kyu",
edades
});

});


module.exports = app;