const {Router}= require('express');
const Div = require('../../models/divisiones/divisiones');
const app = Router();

app.post('/agregar/nueva/division',(req,res)=>{
let body = req.body;

const Divi= new Div({
     division: body.division
});

Divi.save();
res.json({
    ok:true,
    msj:"Se guardo nueva Division",
    Divi
});
});

app.get('/obtener/divisiones', async(req,res)=>{
      
    const Divi =await Div.find();
    res.json({
     ok:true,
     msj:" estos son todos los Divisiones",
     Divi
    });
});

module.exports = app;