const {Router} = require('express');
const Entrenador = require('../../models/entrenadores/entrenadores');
const app = Router();

app.post('/agregar/nuevo/entrenador',(req,res)=>{

let body = req.body;

    let entrena = new Entrenador({
      apellidoMaterno:body.apellidoMaterno,
      apellidoPaterno:body.apellidoPaterno,
      curp:body.curp,
      estado:body.estado,
      estilo:body.estilo,
      fechaNacimiento:body.fechaNacimiento,
      gradoCN:body.gradoCN,
      nombre:body.nombre,
      sexo:body.sexo,
      presidente:body.presidente
      /* campos:body.campos */
    });

    entrena.save();
     res.json({
         ok:true,
         entrena
     })
});


app.get('/obtener/entrenador',async(req,res) =>{

  const respuesta = await Entrenador.find();

  res.json({
      ok:true,
      msj:"aqui estan todos los entrenadores",
      respuesta
  })
});
module.exports = app;