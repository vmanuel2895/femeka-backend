const {Router} = require('express');
const exaNew = require('../../models/examinadores/examinador');
const app = Router();

app.post('/agregar/nuevo/examinador',(req,res)=>{

    let body = req.body;

    let exaNuevo = new exaNew({
        apellidoMaterno: body.apellidoMaterno,
        apellidoPaterno: body.apellidoPaterno,
        curp: body.curp,
        estado: body.estado,
        estilo: body.estilo,
        fechaNacimiento: body.fechaNacimiento,
        gradoCN: body.gradoCN,
        nombre: body.nombre,
        sexo: body.sexo,
        presidente:body.presidente
    /* camposExaminadores: body.camposExaminadores */
    });

    exaNuevo.save();
    res.json({
        ok:true,
        exaNuevo
    });
});

app.get('/obtener/examinadores',async(req,res) =>{

    const respuesta = await exaNew.find();
  
    res.json({
        ok:true,
        msj:"aqui estan todos los examinadores",
        respuesta
    })
  });

module.exports = app;