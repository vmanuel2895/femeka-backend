const {Router} = require('express');
const juezNew = require('../../models/jueces/jueces');
const app = Router();

app.post('/agregar/nuevo/juez',(req,res)=>{

    let body = req.body;

    let jues = new juezNew({
        apellidoMaterno: body.apellidoMaterno,
        apellidoPaterno: body.apellidoPaterno,
        curp: body.curp,
        estado: body.estado,
        estilo: body.estilo,
        fechaNacimiento: body.fechaNacimiento,
        gradoCN: body.gradoCN,
        licencia: body.licencia,
        nombre: body.nombre,
        padecimientoCronico: body.padecimientoCronico,
        sexo: body.sexo,
        tipoSangre: body.tipoSangre,
        presidente:body.presidente
    /* camposJueces: body.camposJueces */
    }); 

    jues.save();

    res.json({
        ok:true,
        jues
    }
    );
});


app.get('/obtener/juces',async(req,res) =>{

    const respuesta = await juezNew.find();

    res.json({
        ok:true,
        msj:"aqui estan todos los jueces",
        respuesta
    })
});
module.exports = app;