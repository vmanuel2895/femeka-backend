const {Router}= require('express');
const katasTabla = require('../../models/katas/katas');
const app = Router();

app.get('/obtener/katas', async (req,res)=>{

    const body = req.body;

    const katas = await katasTabla.find();

    if(katas == "" || katas == null){
        res.status(404).json({
            ok:false,
            msj:"No hay katas",
        })
    }
    
    res.json({
     ok:true,
     msj:"Estos son los katas",
     katas
    });
});

module.exports = app;