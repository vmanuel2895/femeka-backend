const {Router}= require('express');
const categoriaTabla = require('../../models/negrasKumite/kumiteNegrasM');
const app = Router();

app.get('/obtener/categorias/kumite/negra/Mujer', async (req,res)=>{
    const edades = await categoriaTabla.find();
    
    res.json({
     ok:true,
     msj:"Estos son los categorias",
     edades
    });
});

app.post('/agregar/nueva/categoria/kumite/negras/Mujer',(req,res)=>{
  const body = req.body;
    const respuesta = new categoriaTabla({
        categoria : body.categoria
    });

    respuesta.save();
    res.json({
        ok:true,
        msj:"categoria enviada",
        respuesta
    })
});

module.exports = app;