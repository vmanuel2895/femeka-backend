const {Router, response} = require('express');
const Usuario = require('../models/presidentes/presidentes');
const encriptado = require('bcryptjs');
const presidentes = require('../models/presidentes/presidentes');
const {generarJWT}= require('../help/jwt');
const app = Router();

app.post('/login', async(req,res = response)=>{

    const {nombre, password} = req.body;
    // const {body} = req;
try {

   const entrar = await Usuario.findOne({nombre});

   if(!entrar){

   res.status(404).json({
     ok:false,
     msg:"Nombre de usuario no valido"
     });

   }

   //verificar password
   const valiPass = encriptado.compareSync(password, entrar.password);

   if(!valiPass)
   {

    res.status(404).json({
      ok:false,
      msg:"Usuario o Contraseña no son correctos"
      });
 
    }
    // Generar Token

    // const token = await generarJWT(entrar.id);

    res.json({
        ok:true,
        // token,
        entrar,
        mns:"Entramos al login otra vez :D"
    });
} catch (error) {
    console.log(error," error con login");
    res.status(500).json({
        ok:false,
        mns:"EROR AL LOGEAR"
    });
}

});

app.get('/login/usuario', async (req,res)=>{

    let body = req.body;

    presidentes
    .find({ nombre : body.nombre})
    res.json({
        ok:true,
        body,
        msj:"enontre tu presidente"
    });
})

module.exports = app;