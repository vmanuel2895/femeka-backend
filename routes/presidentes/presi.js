const {Router, response} = require('express');
const presidente = require('../../models/presidentes/presidentes');
const RespServer = require('../../fuctions/funciones');
const route = Router();
const {generarJWT} = require('../../help/jwt');
const {validarJWT}= require('../../midelware/validar-token');
const pass = require('bcryptjs');


route.get('/obtener/presidentes',async (req,res)=>{

     const respuesta = await presidente.find();
    res.json({
            ok:true,
             respuesta,
            //  uid: req.uid
          mns:"primer consulta"
        });
})

route.post('/presidente/nuevo',async(req,res)=>{

    let body = req.body;

     let presidentenew = new presidente({
      nombre : body.nombre,
  password : body.password,
  estado: body.estado,
  role: body.role
 });//fin new presidente
    

 /// encriptar contaseña
  const salt = pass.genSaltSync();
  presidentenew.password = pass.hashSync(body.password,salt);
 /////////
presidentenew.save();

// const token = await generarJWT(entrar.id);
    res.json({
            ok:true,
            presidentenew
            // token
        })
})

route.put('/actualizar/usuario/presidente/:id', async (req,res = response )=>{
    const uid = req.params.id;
try {

    const usuarioDB = presidente.findById(uid);

    if(!usuarioDB){
       res.status(404).json({
       ok: false,
       mns:"No existe ningun usuario con ese ID"
       });
    }

    
    const campos = req.body;
    delete campos.estado,
    delete campos.role
    delete campos.nombre

    const salt = pass.genSaltSync();
   campos.password = pass.hashSync(campos.password,salt);
    const updateUser = await presidente.findByIdAndUpdate(uid, campos, { new:true});

    res.json({
        ok:true,
     updateUser
    });


} catch (error) {
    console.log(error);
    res.status(500).json({
      ok:false,
      mns:"Error al actualizar usuario/presidente"
    });
}
});

// variantes de devolver el presidente (peticion de vico)

route.get('/obtener/presidente/:estado', async (req,res) =>{
 const edo = req.params.estado;
 const respuesta = await presidente.find({estado:edo});

    res.json({
        ok:true,
        msj:"Encontre Presidente",
        respuesta
    });
});

route.get('/encontrar/presidente/:id', async (req,res) =>{
    const id = req.params.id;
    const respuesta = await presidente.find({_id:id});
   
       res.json({
           ok:true,
           msj:"Encontre Presidente",
           respuesta
       });
});

route.put('/actualiza/presidente/:id', async (req, res)=>{
    let id = req.params.id;
    const campos = req.body;
    const respuesta = await presidente.findByIdAndUpdate(id,campos,{new:true});
   
    res.json({
        ok:true,
        msj:"Datos Actualizados del Presidente",
        respuesta
    });
});



module.exports = route;