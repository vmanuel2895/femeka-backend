const {Router, response} = require('express');
const renade = require('../../models/renades/renade');
const route = Router();

route.post('/registro/renades',async (req,res)=>{
    
    let body = req.body;

    const counter = await renade.countDocuments() + 1;

    let registroNewRenade = new renade({
        idPresidente : body.idPresidente,
        folio: counter,
        estado: body.estado,
        nombre: body.nombre,
        apellidoPaterno:body.apellidoPaterno,
        apellidoMaterno:body.apellidoMaterno,
        fechaNacimiento:body.fechaNacimiento,
        vigencia:body.vigencia,
        curp:body.curp,
        tipo:body.tipo,
        club:body.club,
        directorClub:body.directorClub,
        direccionClub:body.direccionClub,
        email:body.email,
        telefono:body.telefono,
        edad:body.edad,
        genero:body.genero
    });
        
    registroNewRenade.save();
    res.json({
        ok:true,
        registroNewRenade
    });
});

route.get('/obtener/renade/:id',async(req, res) =>{

    let id = req.params.id;
    const respuesta = await renade.findById(id);

    res.json({
        ok:true,
        respuesta
    });
});

route.put('/actualiza/renade/:id', async (req, res)=>{
    let id = req.params.id;
    const campos = req.body;
    const respuestaUp = await renade.findByIdAndUpdate(id,campos,{new:true});
   
    res.json({
        ok:true,
        msj:"Datos Actualizados del Renade",
        respuestaUp
    });
});

route.delete('/eliminar/renade/:id', async(req, res) => {
    let id = req.params.id;
    const respuesta = await renade.findByIdAndDelete(id);
    res.json({
        ok:true,
        msj:"Dato eliminado del Renade",
        respuesta
    });
});

route.get('/validar/renade/:curp', async(req, res) => {
    let cur = req.params.curp;
    const respuesta = await renade.find({curp:cur});
    res.json({
        ok:true,
        msj:"Se encontro el renade",
        respuesta
    });
});

route.get('/renade/estado/:estado', async(req, res) => {
    let est = req.params.estado;
    const respuesta = await renade.find({estado:est});
    res.json({
        ok:true,
        msj:"Se encontro el renade",
        respuesta
    });
});


module.exports = route;