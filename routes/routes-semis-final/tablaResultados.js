const {Router} = require('express');
const tablaResult = require('../../models/Tablas-semis-final/resultados');
const app = Router();

app.post('/enviar/tabla/Resultados',(req, res)=>{

    const body = req.body;

    const respuesta = new tablaResult({
        idAtleta : body.idAtleta,
        nivelAtletico: body.nivelAtletico,
        nivelTecnico: body.nivelTecnico,
        puntajeTotal: body.puntajeTotal,
        tatami: body.tatami,
        equipo: body.equipo,
        kata: body.kata,
        ronda: body.ronda
    });

    respuesta.save();
    res.json({
        ok:true,
        msj:"Datos Guardados Exitosamente a tabla result",
        respuesta
    })
});

app.get('/obtener/resultados',async (req,res)=>{

    const finalistas = await tablaResult.find()
    .populate('idAtleta')

        if(finalistas == "" || finalistas == null){
            res.json({
                ok:false,
                msj:"No hay resultados",
            })
        }
        
        res.json({
         ok:true,
         msj:"Estos son los resultados de atleltas",
         finalistas
        });
    

});

module.exports = app;