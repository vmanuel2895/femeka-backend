const {Router} = require('express');
const tablaSemiUno = require('../../models/Tablas-semis-final/semiUno');
const app = Router();

app.post('/enviar/tabla/Primer/semi',(req, res)=>{

    const body = req.body;

    const respuesta = new tablaSemiUno({
        idAtleta : body.idAtleta,
        nivelAtletico: body.nivelAtletico,
        nivelTecnico: body.nivelTecnico,
        puntajeTotal: body.puntajeTotal
    });

    respuesta.save();
    res.json({
        ok:true,
        msj:"Datos Guardados Exitosamente a tabla Primer semi final",
        respuesta
    })
});


app.get('/obtener/primeros/semi/finalistas',async (req,res)=>{

    const finalistas = await tablaSemiUno.find()
    .populate('idAtleta')

        // if(finalistas == "" || finalistas == null){
        //     res.status(404).json({
        //         ok:false,
        //         msj:"No hay semi finalistas",
        //     })
        // }
        
        res.json({
         ok:true,
         msj:"Estos son los semi finalistas",
         finalistas
        });
    

});

module.exports = app;