const {Router} = require('express');
const winners = require('../../models/Tablas-semis-final/winners');
const app = Router();

app.post('/enviar/tabla/winners',(req, res)=>{

    const body = req.body;

    const respuesta = new winners({
        idAtleta : body.idAtleta,
        puntajeTotal: body.puntajeTotal,
        lugar : body.lugar,
        rango: body.rango,
        division : body.division
    });

    respuesta.save();
    res.json({
        ok:true,
        msj:"Datos Guardados Exitosamente a tabla ganadores",
        respuesta
    })
});



app.get('/obtener/ganadores',async (req,res)=>{

    const finalistas = await winners.find()
    .populate('idAtleta')
        // if(finalistas == "" || finalistas == null){
        //     res.status(404).json({
        //         ok:false,
        //         msj:"No hay Ganadores",
        //     })
        // }
        
        res.json({
         ok:true,
         msj:"Estos son  lugares ganadores",
         finalistas
        });
    

});

module.exports = app;