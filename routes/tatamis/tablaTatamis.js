const {Router}= require('express');
const Tata = require('../../models/tatamis/tatamis');
const app = Router();

app.post('/agregar/nuevo/tatami',(req,res)=>{
let body = req.body;

const tatamis= new Tata({
     nombre: body.nombre
});

tatamis.save();
res.json({
    ok:true,
    msj:"Se guardo Tatami",
    tatamis
});
});

app.get('/obtener/tatamis',async(req,res)=>{
      
    const tatamis = await Tata.find();
    res.json({
     ok:true,
     msj:" estos son todos los tatamis",
     tatamis
    });
});

module.exports = app;